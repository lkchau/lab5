module arbiter_4_tb ();

  reg reset = 1'b1;
  reg clk = 1'b0;
  reg [3:0] req = 4'b0000;
  wire [3:0] gnt;

  arbiter_4 arbiter_4_dut (.clk(clk),
                           .reset(reset),
                           .req(req),
                           .gnt(gnt));

  initial begin
  #10 reset = 1'b0;
  #10 req[0] <= 1'b1;
  while (gnt[0] != 1'b1) #10;
  #20 req[1] <= 1'b1;
  #20 req[0] <= 1'b0;
  while (gnt[1] != 1'b1) #10;
  #20 req[1] <= 1'b0;
  #20 req[1] <= 1'b1; // Two reqs for 1 and 3 at the same time
      req[3] <= 1'b1;
  #20 req[3] <= 1'b0; // Drop req 3
  #20 req[1] <= 1'b0; // Drop req 1

  #20 req[0] <= 1'b1; // Req for all
      req[1] <= 1'b1;
      req[2] <= 1'b1;
      req[3] <= 1'b1;

  #20 req[3] <= 1'b0; // Drop for 3
  #20 req[1] <= 1'b0; // Drop early for 1
  #20 req[2] <= 1'b0; // Drop for 2
  #20 req[0] <= 1'b0; // Drop for 0

  #20 req[1] <= 1'b1; // Simple test for 1
  #20 req[1] <= 1'b0;

  #20 $finish;
  end

  always #5 clk <= ~clk;

endmodule
