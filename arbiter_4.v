module arbiter_4 (input clk,
                  input reset,
                  input [3:0] req,
                  output reg [3:0] gnt);

always @(posedge clk or posedge reset) begin
  if (reset == 1'b1) begin
    // reset all gnt ffs
    gnt <= 4'b0000;
 end
 else begin // functional block with equations
   gnt[0] <=  (req[0] & ~req[1] & ~req[2] & ~req[3] 
               & ~gnt[0] & ~gnt[1] & ~gnt[2] & ~gnt[3]) // activation term with prio
            | (req[0] & gnt[1] & ~req[1] & ~req[2] & ~req[3]) // skip from 1
            | (req[0] & gnt[2] & ~req[2] & ~req[1] & ~req[3]) // skip from 2
            | (req[0] & gnt[3] & ~req[3] & ~req[1] & ~req[2]) // skip from 3
            | (req[0] & gnt[0]); // hold term for gnt0 until req0 is deasserted
   gnt[1] <= req[1] & ~req[2] & ~req[3] // activation term with prio
             & ~gnt[0] & ~gnt[1] & ~gnt[2] & ~gnt[3]
            | (req[1] & gnt[2] & ~req[2] & ~req[3]) // skip from 2
            | (req[1] & gnt[3] & ~req[3] & ~req[2]) // skip from 3
            | (req[1] & gnt[0] & ~req[0] & ~req[2] & ~req[3]) // skip from 0
            | (req[1] & gnt[1]); // hold term for gnt1 until req1 is deasserted
   gnt[2] <= (req[2] & ~req[3] 
             & ~gnt[0] & ~gnt[1] & ~gnt[2] & ~gnt[3]) // activation term with prio
            | (req[2] & gnt[3] & ~req[3]) // skip from 3
            | (req[2] & gnt[0] & ~req[0] & ~req[3]) // skip from 0
            | (req[2] & gnt[1] & ~req[1] & ~req[3]) // skip from 1
            | (req[2] & gnt[2]); // hold term for gnt2 until req2 is deasserted
   gnt[3] <=  (req[3]
               & ~gnt[0] & ~gnt[1] & ~gnt[2] & ~gnt[3])  // activation term with prio
            | (req[3] & gnt[0] & ~req[0]) // skip from 0
            | (req[3] & gnt[1] & ~req[1]) // skip from 1
            | (req[3] & gnt[2] & ~req[2]) // skip from 2
            | (req[3] & gnt[3]); // hold term for gnt3 until req3 is deasserted
 end
end

endmodule
