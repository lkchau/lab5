module arbiter_4_sva (input logic clk,
                      input logic reset,
                      input logic [3:0] req,
                      output logic [3:0] gnt);

  arbiter_4 arbiter_4_dut (.clk(clk),
                           .reset(reset),
                           .req(req),
                           .gnt(gnt));

  reg init = 1'b1;
  always @(posedge clk) begin
    if (init) assume (reset);
    init <= 1'b0;
  end

  // Make sure that at most one output bit is a 1.
  property output_onehot0;
    @(posedge clk) $onehot0(gnt);
  endproperty
  zonehot: assert property (output_onehot0);


  // Make sure the priorty of requests are correctly granted.
  property prio0;
    @(posedge clk) $rose(gnt[0]) |-> (~$past(req[1]) & ~$past(req[2]) & ~$past(req[3]));
  endproperty
  property prio1;
    @(posedge clk) $rose(gnt[1]) |-> (~$past(req[2]) & ~$past(req[3]));
  endproperty
  property prio2;
    @(posedge clk) $rose(gnt[2]) |-> (~$past(req[3]));
  endproperty

  priority0: assert property (prio0);
  priority1: assert property (prio1);
  priority2: assert property (prio2);

  // Check to make sure that grants are released properly
  // after a request is de-asserted.
  property rel0;
    @(posedge clk) $fell(req[0]) |-> ##1 ~gnt[0];
  endproperty
  property rel1;
    @(posedge clk) $fell(req[1]) |-> ##1 ~gnt[1];
  endproperty
  property rel2;
    @(posedge clk) $fell(req[2]) |-> ##1 ~gnt[2];
  endproperty
  property rel3;
    @(posedge clk) $fell(req[3]) |-> ##1 ~gnt[3];
  endproperty

  release0: assert property (rel0);
  release1: assert property (rel1);
  release2: assert property (rel2);
  release3: assert property (rel3);

endmodule: arbiter_4_sva
